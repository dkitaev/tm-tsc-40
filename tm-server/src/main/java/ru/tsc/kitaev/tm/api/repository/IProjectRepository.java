package ru.tsc.kitaev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO projects (id, name, description, user_id, status, created, start_date)" +
            " VALUES (#{project.id}, #{project.name}, #{project.description}, #{project.userId}, #{project.status}," +
            " #{project.created}, #{project.startDate})")
    void add(
            @NotNull @Param("project") Project project
    );

    @Delete("DELETE FROM projects")
    void clear();

    @Delete("DELETE FROM projects WHERE user_id = #{userId}")
    void clearByUserId(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM projects")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @NotNull
    List<Project> findAll();

    @Select("SELECT * FROM projects WHERE user_id = #{userId}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @NotNull
    List<Project> findAllByUserId(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM projects WHERE user_id = #{userId} ORDER BY #{sort}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @NotNull
    List<Project> findAllBySort(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("sort") String sort
    );

    @Select("SELECT * FROM projects WHERE user_id = #{userId} AND id = #{id}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Nullable
    Project findById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM projects WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @NotNull
    Project findByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    );

    @Delete("DELETE FROM projects WHERE user_id = #{userId} AND id = #{id}")
    void removeById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Delete("DELETE FROM projects WHERE id =" +
            " (SELECT id FROM projects WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void removeByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    );

    @Select("SELECT count(*) AS count FROM projects WHERE user_id = #{userId}")
    @NotNull
    Integer getSize(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM projects WHERE user_id = #{userId} AND name = #{name}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @NotNull
    Project findByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Delete("DELETE FROM projects WHERE user_id = #{userId} AND name = #{name}")
    void removeByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Update("UPDATE projects SET name = #{name}, description = #{description}" +
            " WHERE user_id = #{userId} AND id = #{id}")
    void updateById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("name") String name,
            @NotNull @Param("description") String description
    );

    @Update("UPDATE projects SET name = #{name}, description = #{description}" +
            " WHERE id = (SELECT id FROM projects WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void updateByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index,
            @NotNull @Param("name") String name,
            @NotNull @Param("description") String description
    );

    @Update("UPDATE projects SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND id = #{id}")
    void startById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE id = (SELECT id FROM projects WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void startByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND name = #{name}")
    void startByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND id = #{id}")
    void finishById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE id = (SELECT id FROM projects WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void finishByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND name = #{name}")
    void finishByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND id = #{id}")
    void changeStatusById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE id = (SELECT id FROM projects WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void changeStatusByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE projects SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND name = #{name}")
    void changeStatusByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") String status
    );

}
