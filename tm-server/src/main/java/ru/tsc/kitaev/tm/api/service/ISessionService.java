package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.User;

import java.util.List;

public interface ISessionService {

    void clear();

    @NotNull
    List<Session> findAll();

    @NotNull
    List<Session> findAll(@Nullable final String sort);

    @Nullable
    Session findById(@NotNull final String id);

    @NotNull
    Session findByIndex(@NotNull final Integer index);

    void removeById(@NotNull final String id);

    void removeByIndex(@NotNull final Integer index);

    boolean existsById(@NotNull final String id);

    boolean existsByIndex(final int index);

    int getSize();

    @NotNull
    Session open(@Nullable String login, @Nullable String password);

    @Nullable
    User checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@Nullable Session session, Role role);

    void validate(@Nullable Session session);

    @NotNull
    Session sign(@NotNull Session session);

    void close(@Nullable Session session);

}
