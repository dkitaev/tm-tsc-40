package ru.tsc.kitaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Override
    public void clearTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @WebMethod
    @Override
    @NotNull
    public List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @WebMethod
    @Override
    @NotNull
    public List<Task> findAllTaskSorted(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "sort", partName = "sort") @Nullable final String sort
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId(), sort);
    }

    @WebMethod
    @Override
    @Nullable
    public Task findTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findById(session.getUserId(), id);
    }

    @WebMethod
    @Override
    @NotNull
    public Task findTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    public void removeTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeById(session.getUserId(), id);
    }

    @WebMethod
    @Override
    public void removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    @NotNull
    public Task createTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().create(session.getUserId(), name, description);
    }

    @WebMethod
    @Override
    @NotNull
    public Task findTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public void removeTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public void updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    @Override
    public void updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    @Override
    public void startTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().startById(session.getUserId(), id);
    }

    @WebMethod
    @Override
    public void startTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().startByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    public void startTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().startByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public void finishTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().finishById(session.getUserId(), id);
    }

    @WebMethod
    @Override
    public void finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    public void finishTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().finishByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public void changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().changeStatusById(session.getUserId(), id, status);
    }

    @WebMethod
    @Override
    public void changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @WebMethod
    @Override
    public void changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    public boolean existsTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean existsTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") final int index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().existsByIndex(session.getUserId(), index);
    }

}
